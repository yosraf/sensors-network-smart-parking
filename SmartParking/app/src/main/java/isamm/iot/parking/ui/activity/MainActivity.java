package isamm.iot.parking.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import isamm.iot.parking.R;
import isamm.iot.parking.data.database.entite.MoreStats;
import isamm.iot.parking.data.database.entite.Parc;
import isamm.iot.parking.data.database.entite.Parking;
import isamm.iot.parking.data.database.entite.Place;
import isamm.iot.parking.data.database.entite.Telemtry;
import isamm.iot.parking.data.viewmodel.MainViewModel;
import isamm.iot.parking.databinding.ActivityMainBinding;
import isamm.iot.parking.extra.Config;
import isamm.iot.parking.extra.MyProgressFormatter;
import isamm.iot.parking.ui.adapter.MoreStatsAdapter;
import isamm.iot.parking.ui.adapter.ParkingAdapter;
import isamm.iot.parking.ui.fragment.DialogBotomSheet;


import static isamm.iot.parking.extra.Config.parcs;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private MainViewModel viewModel;
    private Handler handler;
    private int delay = 10 * 1000;//30second
    private ArrayList<Place> places;
    private ArrayList<MoreStats> moreStats;
    private ParkingAdapter adapter;
    private MoreStatsAdapter moreStatsAdapter;
    private BarChart barChart;
    private int prevtelemetry=-100;
    private Parking parking=new Parking();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setMorestats(false);

        barChart = binding.chart;

        binding.telemtry.setProgressFormatter(new MyProgressFormatter());
        binding.telemtry.setMax(100);

        //toolbar ->action bar
        setSupportActionBar((Toolbar) binding.toolbar);

        //viewmodel initilisation
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // init adapter
        places = new ArrayList<>();
        adapter = new ParkingAdapter(places);
        moreStats=new ArrayList<>();
        initmorestats();

        moreStatsAdapter=new MoreStatsAdapter(moreStats);


        //observe last temperature value
        viewModel.getLastTelemtry().observe(this, new Observer<Telemtry>() {
            @Override
            public void onChanged(@Nullable Telemtry telemtry) {
                if (telemtry != null) {
                    prevtelemetry=binding.telemtry.getProgress();
                    binding.telemtry.setProgress(telemtry.getTemp());
                    //change previous temp value
                    MoreStats stats=moreStats.get(0);
                    stats.setValue(prevtelemetry+"°C");
                    moreStats.set(0,stats);
                    moreStatsAdapter.notifyDataSetChanged();

                }
            }
        });
        //observe last 20 values of temperature
        viewModel.getTelemtry().observe(this, new Observer<List<Telemtry>>() {
            @Override
            public void onChanged(@Nullable List<Telemtry> telemtries) {
                if(!telemtries.isEmpty()){
                    if (telemtries.size() > 0 && telemtries.size() < 20) {
                        setBarchart(telemtries);
                        // binding.telemetry.setCurrentValues(telemtries.get(telemtries.size()-1).getTemp());
                    } else {
                        Collections.reverse(telemtries);
                        List<Telemtry> telemtryList = telemtries.subList(0, 20);
                        Collections.reverse(telemtryList);
                        setBarchart(telemtryList);
                    }
                }

            }
        });
        viewModel.getParcLiveData().observe(this, new Observer<Parc>() {
            @Override
            public void onChanged(@Nullable Parc parc) {
                if(parc!=null){
                    Place p=new Place();
                    if(parc.getName().equals("obs1")){
                        parking.setPlace1(parc.getValue());
                        viewModel.setAct(parcs[1]);
                    }else if(parc.getName().equals("obs2")){
                        parking.setPlace2(parc.getValue());
                        viewModel.setAct(parcs[0]);
                        viewModel.insertParc(parking);

                    }else{

                    }
                }
            }
        });
        viewModel.getParc().observe(this, new Observer<Parking>() {
            @Override
            public void onChanged(@Nullable Parking parking) {
                if (parking != null) {
                    parking.totab();

                    MoreStats stats=moreStats.get(1);
                    int notfree=0;
                    for(int i=0;i<2;i++){
                        if(parking.getPlaces()[i]==1){
                            notfree++;
                        }
                    }
                    stats.setValue(""+(2-notfree)+" places");

                    moreStats.set(1,stats);

                    stats=moreStats.get(2);
                    stats.setValue(""+notfree +" places");

                    moreStats.set(2,stats);
                    moreStatsAdapter.notifyDataSetChanged();

                    if (places.isEmpty()) {
                        Random rnd = new Random();
                        for (int i = 0; i < 2; i++) {
                            Place p = new Place();
                            //generate random color
                            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                            p.setColor(color);
                            p.setStat(parking.getPlaces()[i]);
                            places.add(p);
                        }

                    } else {
                        for (int i = 0; i < 2; i++) {

                            Place p = places.get(i);
                            if (p.getStat() != parking.getPlaces()[i]) {
                                p.setStat(parking.getPlaces()[i]);
                                places.set(i, p);
                            }

                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });
        /*

         viewModel.checkUpdates(parcs,"token_osb1");
            viewModel.checkUpdates(parcs,"token_osb2");
         */


        binding.setAdapter(adapter);
        binding.setAdpt(moreStatsAdapter);


        handler = new Handler();
    }

    private void initmorestats(){
        MoreStats stats;
        for (int i=0;i<3;i++){
             stats=new MoreStats();
             stats.setTitle(getResources().getStringArray(R.array.titles)[i]);
             stats.setValue("-");
             stats.setColor(getResources().getIntArray(R.array.linecolors)[i]);
             moreStats.add(stats);
        }


    }


    @Override
    protected void onStart() {
        super.onStart();
        handler.postDelayed(runnableCode, delay);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnableCode);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.menu_stats:
                if (!binding.getMorestats()) {
                    binding.setMorestats(true);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        item.setIcon(getDrawable(R.drawable.ic_celsius_degrees_sign));
                    } else {

                        item.setIcon(getResources().getDrawable(R.drawable.ic_celsius_degrees_sign));
                    }
                } else {
                    binding.setMorestats(false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        item.setIcon(getDrawable(R.drawable.ic_analytics));
                    } else {

                        item.setIcon(getResources().getDrawable(R.drawable.ic_analytics));
                    }

                }
                //invalidateOptionsMenu();
                binding.executePendingBindings();
                return true;

            case R.id.menu_setting:
                DialogBotomSheet bottomSheetDialog = DialogBotomSheet.getInstance();
                bottomSheetDialog.show(getSupportFragmentManager(), "Custom Bottom Sheet");
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    // Define the code block to be executed
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            // Do something here on the main thread
            String telemetry_att[]={"temperature"};
            viewModel.checkUpdates(telemetry_att,"token_telemetry");
            viewModel.setAct(parcs[0]);
            //viewModel.setAct(parcs[1]);
            handler.postDelayed(runnableCode, delay);
        }
    };



    private void setBarchart(List<Telemtry> list) {
        ArrayList<BarEntry> values = new ArrayList<>();
        int len = list.size();
        int colors[]=new int[20];
        for (int i = 0; i < len; i++) {
            int val = list.get(i).getTemp();
            values.add(new BarEntry(i, val));
            if(val<=20){
                colors[i]=getResources().getColor(R.color.colorblue);
            }else if(val>20 && val <40){
                colors[i]=getResources().getColor(R.color.colorviolet);
            }else{
                colors[i]=getResources().getColor(R.color.colorpinky);
            }
        }
        BarDataSet set1;

        if (barChart.getData() != null &&
                barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.setColors(colors);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(values, "Telemetry stats");
            set1.setColors(colors);
            set1.setDrawValues(false);
            set1.setLabel("Last 20 values Telemetry");

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            barChart.setData(data);
            //barChart.setFitBars(true);
        }

        barChart.invalidate();
    }

}
