package isamm.iot.parking.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import isamm.iot.parking.R;

public class DialogBotomSheet extends BottomSheetDialogFragment {

    public static DialogBotomSheet getInstance() {
        return new DialogBotomSheet();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.bottom_sheet, container, false);

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, new SettingFragment())
                .commit();

        return view;
    }
}
