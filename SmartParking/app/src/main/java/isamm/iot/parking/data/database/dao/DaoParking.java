package isamm.iot.parking.data.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import isamm.iot.parking.data.database.entite.Parking;
@Dao
public interface DaoParking extends DaoApp<Parking> {

    @Query("select * from Parking where id=(select max(id) from parking)")
    LiveData<Parking> getParc();
    @Query("select * from Parking where id=(select max(id) from parking)")
    Parking getLastParc();

}
