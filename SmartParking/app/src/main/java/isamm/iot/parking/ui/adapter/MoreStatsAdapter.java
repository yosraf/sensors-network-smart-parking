package isamm.iot.parking.ui.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import isamm.iot.parking.BR;
import isamm.iot.parking.R;
import isamm.iot.parking.data.database.entite.MoreStats;

public class MoreStatsAdapter  extends RecyclerView.Adapter<MoreStatsAdapter.MyViewHolder> {

    private List<MoreStats> statsList;

    public MoreStatsAdapter(List<MoreStats> statsList) {
        this.statsList = statsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        ViewDataBinding binding= DataBindingUtil.inflate(inflater, R.layout.item_more_stats,viewGroup,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
            MoreStats stats=statsList.get(i);
            myViewHolder.bind(stats);
    }

    @Override
    public int getItemCount() {
        return statsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding Binding;
        public MyViewHolder(@NonNull ViewDataBinding dataBinding) {
            super(dataBinding.getRoot());
            this.Binding=dataBinding;
        }
        public void bind(MoreStats stats){
            Binding.setVariable(BR.stats,stats);
        }
    }
}
