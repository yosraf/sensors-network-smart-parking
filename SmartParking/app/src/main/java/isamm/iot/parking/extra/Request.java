package isamm.iot.parking.extra;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.Executors;


public class Request {



    private static Request instance;

    public static Request getInstance(Context context){
        if(instance==null){
            synchronized (Request.class){
                instance=new Request(context) ;
            }
        }
        return instance;
    }

    public Request(Context context) {
        AndroidNetworking.initialize(context);
    }

    public void Abondonne(){
        AndroidNetworking.cancelAll();
    }

    public ANRequest GET(String URL, Priority priority, @Nullable HashMap<String,String>params, @Nullable String head){

        Log.e("url",URL);
        ANRequest.GetRequestBuilder get= AndroidNetworking.get(URL);
        if(params!=null){

                //get=get.addPathParameter(params);
                get=get.addQueryParameter(params);

        }

        if(head!=null){
            get=get.addHeaders("Authorization",head);
        }


        return get.setPriority(priority)
                .build();
    }

    public ANRequest GETWithExecutor(String URL, Priority priority, @Nullable HashMap<String,String>params, @Nullable String head){

        Log.e("url",URL);
        ANRequest.GetRequestBuilder get= AndroidNetworking.get(URL);
        if(params!=null){
            //get=get.addPathParameter(params);
            get=get.addQueryParameter(params);

        }

        if(head!=null){
            get=get.addHeaders("Authorization",head);
        }


        return get.setPriority(priority).setExecutor(Executors.newSingleThreadExecutor())
                .build();
    }
    public ANRequest GET(String URL, Priority priority, @Nullable String head) {

        ANRequest.GetRequestBuilder get= AndroidNetworking.get(URL);
        if(head!=null){
            get=get.addHeaders("Authorization",head);
        }


        return get.setPriority(priority)
                .build();
    }




}
