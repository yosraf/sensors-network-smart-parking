package isamm.iot.parking.data.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import java.util.List;

import isamm.iot.parking.data.database.entite.Parc;
import isamm.iot.parking.data.database.entite.Parking;
import isamm.iot.parking.data.database.entite.Telemtry;
import isamm.iot.parking.data.repository.MainRepository;

public class MainViewModel extends AndroidViewModel {


    private MainRepository repository;



    private LiveData<Parc> parcLiveData;
    private MutableLiveData<String> att;
    public MainViewModel(@NonNull Application application) {
        super(application);
        repository=new MainRepository(application);
        parcLiveData=new MutableLiveData<>();
        att=new MutableLiveData<>();


        parcLiveData= Transformations.switchMap(att,(d)->{
            if(d.equals("obs1")){
                return repository.getObstacleValue(d,"token_osb1");
            }else if (d.equals("obs2")){
                return repository.getObstacleValue(d,"token_osb2");
            }else{
                return  null;
            }

        });
    }
    public void insertParc(Parking parking){
        repository.InserPart(parking);
    }

    public  LiveData<Parc> getParcLiveData(){
        return parcLiveData;
    }
    public void setAct(String s){
        att.setValue(s);
    }
    public LiveData<List<Telemtry>> getTelemtry(){
        return repository.AllTelemtry();
    }

    public LiveData<Telemtry> getLastTelemtry(){
        return repository.lastTelemtry();
    }

    public LiveData<Parking> getParc(){
        return repository.parkingstatus();
    }

    public void checkUpdates(String[] atts,String tokenkey){
        repository.RetreiveAtt(atts,tokenkey);
    }

}
