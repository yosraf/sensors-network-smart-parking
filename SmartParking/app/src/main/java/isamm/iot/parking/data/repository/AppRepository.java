package isamm.iot.parking.data.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import isamm.iot.parking.data.database.AppRoom;
import isamm.iot.parking.data.database.entite.Parc;
import isamm.iot.parking.data.database.entite.Parking;
import isamm.iot.parking.data.database.entite.Telemtry;
import isamm.iot.parking.extra.Request;

import static isamm.iot.parking.extra.Config.parcs;
import static isamm.iot.parking.extra.Config.server;

public abstract class AppRepository {

    protected Request request;
    protected AppRoom room;
    protected SharedPreferences sharedPreferences;
    public AppRepository(Application application) {
        request= Request.getInstance(application);
        room= AppRoom.getDatabase(application);
         sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(application /* Activity context */);


    }

    private void InsertTelemetry(Telemtry telemtry){
        new AsyncTask<Telemtry,Void,Void>(){

            @Override
            protected Void doInBackground(Telemtry... telemtries) {
                room.daoTelemetry().Insert(telemtries[0]);
                return null;
            }
        }.execute(telemtry);
    }

    private void InsertParking(Parking parking){
        new AsyncTask<Parking,Void,Void>(){

            @Override
            protected Void doInBackground(Parking... parkings) {
                room.daoParking().Insert(parkings[0]);
                return null;
            }
        }.execute(parking);
    }


    private HashMap<String,String> params(String[] atts){
        HashMap<String, String> params = new HashMap<>();
        String value=atts[0];
            if(atts.length>1){
                for(int i=1;i<atts.length;i++){
                    value=value+","+atts[i];
                }
            }

            params.put("clientKeys", value);



        return params;
    }
    // retreive all telemetry from local database
    public LiveData<List<Telemtry>> AllTelemtry(){
        return room.daoTelemetry().getAll();
    }
    // get last telemetry in local database
    public LiveData<Telemtry> lastTelemtry(){
        return room.daoTelemetry().getLastTelemtry();
    }
    // retreive last stats of parking
    public LiveData<Parking> parkingstatus(){
        return room.daoParking().getParc();
    }

    public void RetreiveAtt(String[] atts, final String token_key){

        final String token=sharedPreferences.getString(token_key,"token");//UXsHz8f57piluqr60u1I
        String ip = sharedPreferences.getString("server","");
        HashMap<String,String> map=params(atts);
        String url="http://"+ip+server+token+"/attributes";
        request.GETWithExecutor(url, Priority.HIGH,map,null).getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.has("client")){

                        JSONObject object=response.getJSONObject("client");
                        Log.v("data",object.toString());

                        if(token_key.equals("token_telemetry")){
                            if(object.has("temperature")){

                                int temperature=  object.getInt("temperature");
                                Telemtry telemtry=new Telemtry();
                                telemtry.setTemp(temperature);
                                InsertTelemetry(telemtry);
                            }
                        }


                    }
                } catch (JSONException e) {
                   Log.e("error parse",e.getMessage());
                }
            }

            @Override
            public void onError(ANError anError) {

                Log.e("error parse",anError.getMessage());
            }
        });
    }

    public MutableLiveData<Parc> getObstacleValue(String atts, final String token_key){
        final MutableLiveData<Parc> data=new MutableLiveData<>();
        final String token=sharedPreferences.getString(token_key,"token");//UXsHz8f57piluqr60u1I
        String ip = sharedPreferences.getString("server","");

        String url="http://"+ip+server+token+"/attributes?clientKeys="+atts;
        request.GETWithExecutor(url, Priority.HIGH,null,null).getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.has("client")){

                        JSONObject object=response.getJSONObject("client");
                        Log.v("data",object.toString());
                        Parc parc=new Parc();
                        if (token_key.equals("token_osb1")){

                           parc.setName(parcs[0]);
                           parc.setValue(object.getInt(parcs[0]));


                        }else if (token_key.equals("token_osb2")){

                            parc.setName(parcs[1]);
                            parc.setValue(object.getInt(parcs[1]));

                        }
                        data.postValue(parc);


                    }
                } catch (JSONException e) {
                    Log.e("error parse",e.getMessage());
                }
            }

            @Override
            public void onError(ANError anError) {

                Log.e("error parse",anError.getMessage());
            }
        });

        return data;
    }
    public void InserPart(Parking parking){
        InsertParking(parking);
    }
}
