package isamm.iot.parking.ui.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Random;

import isamm.iot.parking.BR;
import isamm.iot.parking.R;
import isamm.iot.parking.data.database.entite.Place;

public class ParkingAdapter extends RecyclerView.Adapter<ParkingAdapter.MyViewHolder> {


    private List<Place> places;

    public ParkingAdapter(List<Place> places) {
        this.places = places;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(inflater, R.layout.item_place, viewGroup, false);
        return new MyViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Place place = places.get(i);



        place.setNumber(i + 1);
        place.setDirection((i+1) % 2);


        myViewHolder.bind(place,i);

    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding dataBinding;

        public MyViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            dataBinding = binding;
        }

        public void bind(Place p,int position) {
            dataBinding.setVariable(BR.place, p);
            dataBinding.setVariable(BR.position, position);
        }
    }
}
