package isamm.iot.parking.data.database.entite;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

@Entity
public class Parking {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "place_1")
    private int place1;
    @ColumnInfo(name = "place_2")
    private int place2;

    @Ignore
    private int places[]={0,0};

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlace1() {
        return place1;
    }

    public void setPlace1(int place1) {
        this.place1 = place1;
    }

    public int getPlace2() {
        return place2;
    }

    public void setPlace2(int place2) {
        this.place2 = place2;
    }


    public void ChangePlace(int v,int p){
        places[p]=v;

    }
    public void tovariable(){
        place1=places[0];
        place2=places[1];
    }
    public void totab(){
        places[0]=place1;
        places[1]=place2;
    }
    public int[] getPlaces() {
        return places;
    }

    public void setPlaces(int[] places) {
        this.places = places;
    }
}
