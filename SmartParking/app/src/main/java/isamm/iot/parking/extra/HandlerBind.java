package isamm.iot.parking.extra;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.media.Image;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HandlerBind {



    @BindingAdapter("alignementtext")
    public static void settextAlignement(TextView view, int direction){
        if(direction==0){
            view.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        }else{
            view.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        }
    }
    @BindingAdapter("rotation")
    public static void rotation(ImageView view,int direction){
        if(direction!=0){
            view.setRotation(180);
        }else{
            view.setRotation(0);
        }
    }
    @BindingAdapter("tint")
    public static void setColor(ImageView view, int color){
        view.setColorFilter(color);
    }

    @BindingAdapter("customadapter")
    public static void setCustomAdapter(RecyclerView view, RecyclerView.Adapter adapter){

        GridLayoutManager manager = new GridLayoutManager(view.getRootView().getContext(), 2,GridLayoutManager.VERTICAL,false);
        view.setItemAnimator(new DefaultItemAnimator());
        view.setLayoutManager(manager);
        view.setAdapter(adapter);
    }
    @BindingAdapter("Moreadapter")
    public static void setAdapter(RecyclerView view, RecyclerView.Adapter adapter){

        LinearLayoutManager manager = new LinearLayoutManager(view.getRootView().getContext(), LinearLayoutManager.VERTICAL,false);
        view.setItemAnimator(new DefaultItemAnimator());
        view.setLayoutManager(manager);
        view.setAdapter(adapter);
    }

    @BindingAdapter("color")
    public static void setbackground(View view,int color){
        if(color!=0){
            view.setBackgroundColor(color);
        }

    }
}
