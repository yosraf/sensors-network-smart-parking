from digi.xbee.devices import XBeeDevice
from digi.xbee.io import IOLine, IOValue
import requests
import json
from twilio.rest import Client
from sendgrid import SendGridAPIClient, sendgrid, Email
from sendgrid.helpers.mail import Mail, Content


def sendTempData(temp, ip):
    url = ip + ":8080/api/v1/kEjBfuUZ9FN103TImLru/attributes"
    res = requests.post(url,
                        data=json.dumps({'temperature': temp}))
    print(res)


def sendMotionData(motion, ip):
    url = ip + ":8080/api/v1/OpLPWiypKKUShau89X3D/attributes"
    res = requests.post(url,
                        data=json.dumps({'motion': motion}))
    print(res)


def sendObsyacleData(osb_token, data, attr, ip):
    url = ip + ":8080/api/v1/" + osb_token + "/attributes"
    res = requests.post(url=url, data=json.dumps({attr: data}))
    print(res)


def sendsms(sms):
    account_sid = 'ACc07a3f121a5e1862650360229c094dda'
    auth_token = 'ab355a5549b0481b6187a2d7b63eddc5'
    client = Client(account_sid, auth_token)

    message = client.messages \
        .create(
        body=sms,
        from_='+17246029716',
        to='+21658784044'
    )
    print(message)


def sendMail(email):
    api_key = "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"
    sg = sendgrid.SendGridAPIClient(apikey=api_key)
    from_email = Email("yfatnassi20@gmail.com")
    to_email = Email("yosrafatnassi6@gmail.com")
    content = Content("text/plain", email)
    mail = Mail(from_email, "Smart parking status", to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())


if __name__ == "__main__":
    device = XBeeDevice("COM15", 9600)
    device.open()
    mv = 0
    temp = -1
    motion = IOLine.DIO1_AD1
    temppin = IOLine.DIO2_AD2
    obs_1 = IOLine.DIO3_AD3
    obs_2 = IOLine.DIO0_AD0


    def io_sample_callback(io_sample, remote_xbee, send_time):
        ip = "http://192.168.43.133"
       # print("IO sample received at time %s." % str(send_time))
        print("MAC ADDRESS OF THE DEVICE", remote_xbee)

        #
        if (io_sample.has_analog_value(temppin)):
            t = io_sample.get_analog_value(temppin)
            temp = (((t / 1023) * 1200) - 500) / 10
        else:
            temp = 20
        mv = io_sample.get_digital_value(motion)
        abs_1 = io_sample.get_digital_value(obs_1)
        abs_2 = io_sample.get_digital_value(obs_2)
        sendTempData(temp, ip)
        """if( temp>40):
            sendTempData(temp,ip)
            sendMail("Parking is on fire , please call 911")"""
        if (mv == IOValue.LOW):
            sendMotionData(0, ip)
        else:
            sendMotionData(1, ip)
            if (abs_1 == IOValue.HIGH and abs_2 == IOValue.HIGH):
                sendObsyacleData("kmlt5saScj0glpah57YO", 1, "obs1", ip)
                sendObsyacleData("PfrdBnIP4ytawnI8rJAm", 1, "obs2", ip)
                sendsms("The parking lot is full , please come back later")
                sendMail("The parking lot is full , please come back later")
            else:

                sendsms("New car is coming , open the gate")
        if (abs_1 == IOValue.LOW):
            sendObsyacleData("kmlt5saScj0glpah57YO", 0, "obs1", ip)
        else:
            sendObsyacleData("kmlt5saScj0glpah57YO", 1, "obs1", ip)
        if (abs_2 == IOValue.LOW):
            sendObsyacleData("PfrdBnIP4ytawnI8rJAm", 0, "obs2", ip)
        else:
            sendObsyacleData("PfrdBnIP4ytawnI8rJAm", 1, "obs2", ip)


    # Subscribe to IO samples reception.
    device.add_io_sample_received_callback(io_sample_callback)
