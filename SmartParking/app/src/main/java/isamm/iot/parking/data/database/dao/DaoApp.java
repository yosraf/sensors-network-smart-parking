package isamm.iot.parking.data.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
@Dao
public interface DaoApp<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void Insert(T obj);

    @Update
    void Update(T obj);
    @Delete
    void Delete(T obj);

}
