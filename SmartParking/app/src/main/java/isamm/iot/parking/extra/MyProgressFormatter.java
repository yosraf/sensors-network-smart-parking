package isamm.iot.parking.extra;

import com.dinuscxj.progressbar.CircleProgressBar;

public class MyProgressFormatter implements CircleProgressBar.ProgressFormatter {


    @Override
    public CharSequence format(int progress, int max) {
        return  progress+"°C";
    }
}
