package isamm.iot.parking.data.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import isamm.iot.parking.data.database.dao.DaoParking;
import isamm.iot.parking.data.database.dao.DaoTelemetry;
import isamm.iot.parking.data.database.entite.Parking;
import isamm.iot.parking.data.database.entite.Telemtry;


@Database(entities = {Telemtry.class, Parking.class}, version = 3)
public abstract class AppRoom extends RoomDatabase {


    public abstract DaoTelemetry daoTelemetry();
    public abstract DaoParking daoParking();
    private static volatile AppRoom INSTANCE;

    public static AppRoom getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppRoom.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppRoom.class, "parking_database").addMigrations(MIGRATION_1_2).addMigrations(MIGRATION_2_3)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            database.execSQL("CREATE TABLE   Parking( id INTEGER PRIMARY KEY AUTOINCREMENT,place_1 INTEGER ,place_2 INTEGER,place_3 INTEGER,place_4 INTEGER)");
        }
    };
    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            database.execSQL("DROP TABLE Parking");
            database.execSQL("CREATE TABLE   Parking( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,place_1 INTEGER NOT NULL,place_2 INTEGER NOT NULL)");
        }
    };
    }
