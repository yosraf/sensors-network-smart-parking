#include <Servo.h>
Servo monServomoteur;
int pir_sensor=7;
int pir_output=6;
int motion;
int ob_1_pin=8;
int ob_2_pin=9;
int ob_1_out=5;
int ob_2_out=4;
int obst_1;
int obst_2;
int led_out=11;
int buzzer_out=12;
void setup() {
  // put your setup code here, to run once:
 Serial.begin(115200);
 pinMode(pir_sensor, INPUT);
 monServomoteur.attach(10);
 pinMode(pir_output, OUTPUT);
 pinMode(ob_1_pin, INPUT);
 pinMode(ob_2_pin, INPUT);
 pinMode(ob_1_out, OUTPUT);
 pinMode(ob_2_out, OUTPUT);
 pinMode(led_out,OUTPUT);
 pinMode(buzzer_out,OUTPUT);
 monServomoteur.write(180);

}
void loop() {
  obst_1=digitalRead(ob_1_pin);
  obst_2=digitalRead(ob_2_pin);
  
  if(obst_1==0){
    digitalWrite(ob_1_out,1);
    
  }
  else{
    digitalWrite(ob_1_out,0);
  }
  if(obst_2==0){
    digitalWrite(ob_2_out,1);
  }
  else{
    digitalWrite(ob_2_out,0);
  }

  motion=digitalRead(pir_sensor);
  Serial.println(obst_1);
  if(motion==0){
    digitalWrite(pir_output,0);
    noTone(buzzer_out );
    monServomoteur.write(180);

  }
  else{
     digitalWrite(pir_output,1);
    if(obst_1==0 && obst_2==0){
      monServomoteur.write(180);
      Serial.println("no place to park");
      noTone(buzzer_out );
    }
    else
   
    { 
     tone(buzzer_out, 1000);
     delay(1000);       
     noTone(buzzer_out );
     monServomoteur.write(90);
     }
     
  
  }
if(obst_1==0 && obst_2==0){
    digitalWrite(led_out,HIGH);
    
  }
 else{
  digitalWrite(led_out,LOW);
 }
     delay(100);
  }

