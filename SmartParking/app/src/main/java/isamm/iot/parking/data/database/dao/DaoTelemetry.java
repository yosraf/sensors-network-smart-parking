package isamm.iot.parking.data.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import isamm.iot.parking.data.database.entite.Telemtry;
@Dao
public interface DaoTelemetry extends DaoApp<Telemtry> {


    @Query("select * from Telemtry")
    LiveData<List<Telemtry>> getAll();

    @Query("select * from Telemtry where id=(select max(id) from telemtry)")
    LiveData<Telemtry> getLastTelemtry();

}
